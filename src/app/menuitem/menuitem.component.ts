import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-menuitem',
  templateUrl: './menuitem.component.html',
  styleUrls: ['./menuitem.component.css']
})
export class MenuitemComponent implements OnInit {

  @Input() menuItem:any;

  constructor() { }

  ngOnInit() {
  }

  addMenuItem(event){
    this.menuItem.children.push(
      {
        "mesh": "",
        "name": "test",
        "Description": "",
        "showLabel": false,
        "isAnimation": false,
        "animationTrigger": "",
        "steps":[],
        "children":[]
      }
    );
  }

}
