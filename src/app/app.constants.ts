import { environment } from '../environments/environment';
var host_dev = "http://localhost:3000";
var host_prod =  "http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com:3000";
export const MY_HOST = (environment.production ? host_prod : host_dev);
