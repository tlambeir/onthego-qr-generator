import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodeUsersComponent } from './qrcode-users.component.ts';

describe('QrcodeUsersComponent', () => {
  let component: QrcodeUsersComponent;
  let fixture: ComponentFixture<QrcodeUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodeUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodeUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
