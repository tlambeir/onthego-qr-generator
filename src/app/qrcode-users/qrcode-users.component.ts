import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';
import {MatDialog} from '@angular/material/dialog';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'dialog-content-users-dialog',
  templateUrl: './dialog-content-users-dialog.html',
})
export class DialogContentUsersDialog {}

@Component({
  selector: 'app-qrcode-users',
  templateUrl: './qrcode-users.component.html',
  styleUrls: ['./qrcode-users.component.css']
})
export class QrcodeUsersComponent implements OnInit {

  users: any;
  currentPage:number;
  numOfResults:any;
  pages:any;
  constructor(private router: Router, private authService: AuthService, private http: HttpClient, public dialog: MatDialog) { }

  ngOnInit() {
    this.authService.getLoggedin().subscribe((resp: boolean) => {
      if(resp){
        this.currentPage=1;
        this.load();
      }
      else {
        // redirect to login page
        this.router.navigate(['/login']);
      }
    });
  }

  openDialog(id) {
    const dialogRef = this.dialog.open(DialogContentUsersDialog);

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.http.delete(MY_HOST + '/users/' +id).subscribe((data: CustomResponse) => {
          console.log(`Delete result: ${data}`);
          this.reload();
        });
      }
    });
  }

  reload(){
    this.load();
  }
  setPage(page: number) {
    // get pager object from service
    this.currentPage = page;

    // get current page of items
    this.load();
  }
  load(){
    this.http.get(MY_HOST + '/users?page='+this.currentPage).subscribe((data: CustomResponse) => {
      this.users = data.users;
      this.currentPage = data.currentPage;
      this.numOfResults= data.numOfResults;
      this.pages= Array(data.pages).fill(0).map((x,i)=>i);
    });
  }
}
export interface CustomResponse {
  users: any;
  currentPage:any;
  numOfResults:any;
  pages:any;
}
