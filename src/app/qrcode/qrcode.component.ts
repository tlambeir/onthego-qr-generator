import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';
import { QrcodeCreateComponent } from '../qrcode-create/qrcode-create.component';
import *  as QRCode from 'qrcode'

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css']
})
export class QrcodeComponent implements OnInit {

  qrcodes: any;
  opts: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.opts = {
      errorCorrectionLevel: 'H',
      type: 'image/jpeg',
      rendererOpts: {
        quality: 0.3,
        width:244
      }
    };
  }

  reload(){
    this.load();
  }

  load(){
    this.http.get(MY_HOST + '/qrcode?limit=1').subscribe(data => {
      this.qrcodes = data;
      for(var i=0; i < this.qrcodes.length; i++ ){
        var qrcode = this.qrcodes[i];
        QRCode.toDataURL(MY_HOST + '/qrcode' + "/" + qrcode._id, this.opts, function (err, url) {
          qrcode.qrcode = url;
        })
      }
    });
  }

}
