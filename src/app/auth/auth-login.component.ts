import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.css']
})
export class AuthLoginComponent implements OnInit {

   constructor(private router: Router,private authService : AuthService, private http: HttpClient) { }

  ngOnInit() {
  }

  Authenticate(event){
    this.authService.Authenticate().subscribe((resp: boolean) => {
      if(resp){
        this.router.navigate(['/']);
      }
      else {
        // show error
      }
    });
  }
}