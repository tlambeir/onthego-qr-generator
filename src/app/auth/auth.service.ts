import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }

  getLoggedin() {
    return this.http.get(MY_HOST + '/auth' + '/loggedin', {
      withCredentials: true  // <=========== important!
    });
  }

  Authenticate(){
    return this.http.post(MY_HOST + '/auth' + '/login', {
      withCredentials: true,  // <=========== important!
      email: 'thomas.lambeir.ext@nokia.com'
    });
  }

  logout(){
    return this.http.get(MY_HOST + '/auth' + '/logout', {
      withCredentials: true  // <=========== important!
    });
  }

}