import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ng-menuitem-detail',
  templateUrl: './menuitem-detail.component.html',
  styleUrls: ['./menuitem-detail.component.css']
})
export class MenuitemDetailComponent implements OnInit {

  @Input() menuItem:any;

  constructor() { }

  ngOnInit() {
  }

}
