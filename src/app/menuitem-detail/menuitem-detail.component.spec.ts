import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuitemDetailComponent } from './menuitem-detail.component';

describe('MenuitemDetailComponent', () => {
  let component: MenuitemDetailComponent;
  let fixture: ComponentFixture<MenuitemDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuitemDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuitemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
