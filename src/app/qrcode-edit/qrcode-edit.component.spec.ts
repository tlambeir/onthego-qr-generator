import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodeEditComponent } from './qrcode-edit.component';

describe('QrcodeEditComponent', () => {
  let component: QrcodeEditComponent;
  let fixture: ComponentFixture<QrcodeEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodeEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
