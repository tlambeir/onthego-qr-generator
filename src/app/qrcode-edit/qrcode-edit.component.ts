import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';

@Component({
  selector: 'app-qrcode-edit',
  templateUrl: './qrcode-edit.component.html',
  styleUrls: ['./qrcode-edit.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class QrcodeEditComponent implements OnInit {

  qrcode : any = {
    structure:[]
  };

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getQrcode(this.route.snapshot.params['id']);
  }

  getQrcode(id) {
    this.http.get(MY_HOST + '/qrcode/'+id).subscribe(data => {
      this.qrcode = data;
    });
  }

    updateQrcode(id, data) {
        this.qrcode.updated_date = Date.now();
        this.http.put(MY_HOST + '/qrcode/'+id, this.qrcode).subscribe(res => {
              let id = res['_id'];
              this.router.navigate(['/list']);
            }, (err) => {
              console.log(err);
            }
        );
  }

  addMenuItem(event){
    this.qrcode.structure.push(
      {
        "mesh": "",
        "name": "test",
        "Description": "",
        "showLabel": false,
        "isAnimation": false,
        "animationTrigger": "",
        "steps":[],
        "children":[]
      }
    );
  }

}