import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { QrcodeComponent } from './qrcode/qrcode.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';


import { RouterModule, Routes } from '@angular/router';
import { QrcodeDetailComponent } from './qrcode-detail/qrcode-detail.component';
import { QrcodeCreateComponent } from './qrcode-create/qrcode-create.component';
import { QrcodeEditComponent } from './qrcode-edit/qrcode-edit.component';
import { QrcodeUsersEditComponent } from './qrcode-user-edit/qrcode-users-edit.component';
import { QrcodeListComponent } from './qrcode-list/qrcode-list.component';
import { DialogContentExampleDialog } from './qrcode-list/qrcode-list.component';
import { QrcodeUsersComponent } from './qrcode-users/qrcode-users.component';
import { DialogContentUsersDialog } from './qrcode-users/qrcode-users.component';

import { BookComponent } from './book/book.component';
import { MenuitemComponent } from './menuitem/menuitem.component';
import { MenuComponent } from './menu/menu.component';
import { MenuitemDetailComponent } from './menuitem-detail/menuitem-detail.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AuthService} from "./auth/auth.service";
import { AuthLoginComponent } from './auth/auth-login.component';


const appRoutes: Routes = <Routes>[
  {
    path:'login',
    component: AuthLoginComponent
  },
  {
    path: 'qrcodes',
    component: QrcodeComponent,
    data: {title: 'Qrcode List'}
  },
  {
    path: 'list',
    component: QrcodeListComponent,
    data: {title: 'Qrcode List'}
  },
  {
    path: 'users',
    component: QrcodeUsersComponent,
    data: {title: 'Users'}
  },
  {
    path: 'qrcode-details/:id',
    component: QrcodeDetailComponent,
    data: {title: 'Qrcode Details'}
  },
  {
    path: 'qrcode-create',
    component: QrcodeCreateComponent,
    data: {title: 'Create Qrcode'}
  },
  {
    path: 'qrcode-edit/:id',
    component: QrcodeEditComponent,
    data: {title: 'Edit Qrcode'},
    pathMatch: 'full'
  },
  {
    path: 'user',
    component: QrcodeUsersEditComponent,
    data: {title: 'Edit User'},
    pathMatch: 'full'
  },
  {
    path: 'user/:id',
    component: QrcodeUsersEditComponent,
    data: {title: 'Edit User'},
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/qrcodes',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    QrcodeComponent,
    QrcodeDetailComponent,
    QrcodeCreateComponent,
    QrcodeEditComponent,
    QrcodeUsersEditComponent,
    QrcodeListComponent,
    QrcodeUsersComponent,
    BookComponent,
    MenuitemComponent,
    MenuComponent,
    MenuitemDetailComponent,
    DialogContentExampleDialog,
    DialogContentUsersDialog,
    AuthLoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    RouterModule.forRoot
    (
        appRoutes,
        {
          useHash: true,
          enableTracing: true
        } // <-- debugging purposes only
    )
  ],
  entryComponents: [
    DialogContentExampleDialog,
    DialogContentUsersDialog
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
