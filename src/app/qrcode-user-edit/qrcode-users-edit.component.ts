import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';

@Component({
  selector: 'app-qrcode-edit',
  templateUrl: './qrcode-users-edit.component.html',
  styleUrls: ['./qrcode-users-edit.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class QrcodeUsersEditComponent implements OnInit {

  user : any = {};

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    if(this.route.snapshot.params['id'])
      this.getUser(this.route.snapshot.params['id']);
  }

  getUser(id) {
    this.http.get(MY_HOST + '/users/'+id).subscribe(data => {
      this.user = data;
    });
  }

  updateOrdAddUser(id,data){
    id ? this.updateUser(id,data) : this.addUser(data);
  }

  addUser(data) {
    this.user.updated_date = Date.now();
    this.http.post(MY_HOST + '/users', this.user).subscribe(res => {
        this.router.navigate(['/users']);
      }, (err) => {
        console.log(err);
      }
    );
  }

  updateUser(id, data) {
        this.user.updated_date = Date.now();
        this.http.put(MY_HOST + '/users/'+id, this.user).subscribe(res => {
              this.router.navigate(['/users']);
            }, (err) => {
              console.log(err);
            }
        );
  }

}