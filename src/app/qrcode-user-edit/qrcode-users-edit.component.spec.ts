import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodeUsersEditComponent } from './qrcode-edit.component';

describe('QrcodeUsersEditComponent', () => {
  let component: QrcodeUsersEditComponent;
  let fixture: ComponentFixture<QrcodeUsersEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodeUsersEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodeUsersEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
