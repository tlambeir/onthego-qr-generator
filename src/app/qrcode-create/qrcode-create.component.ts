import { Component, OnInit, ViewEncapsulation,ElementRef, Input, Output,EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { MenuitemComponent } from '../menuitem/menuitem.component';
import { MY_HOST } from '../app.constants';
import *  as QRCode from 'qrcode'
import {MatRadioButton} from '@angular/material/radio';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-qrcode-create',
  templateUrl: './qrcode-create.component.html',
  styleUrls: ['./qrcode-create.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class QrcodeCreateComponent implements OnInit {

  @Output() QRCodeSavedEvent:EventEmitter<any> = new EventEmitter();

  formData = new FormData();

  qrcodes: any;
  opts: any;
  time:any;

  newqrcode:any;

  qrcode = {
    apptype: "",
    duration:"",
    scene:"",
    model:"",
    structure:[],
    type:"",
    storageLocation:'1'
  };

  isAdvanced = false;

  showFileToolTip = false;

  hideFileToolTip(){
    this.showFileToolTip = false;
  }
  enableFileToolTip(){
    this.showFileToolTip = true;
  }

  constructor(private authService: AuthService, private http: HttpClient, private router: Router, private el: ElementRef) { }

  ngOnInit() {
    this.authService.getLoggedin().subscribe((resp: boolean) => {
      if(resp){
        this.resetQRCode();
        this.opts = {
          errorCorrectionLevel: 'H',
          type: 'image/jpeg',
          rendererOpts: {
            quality: 0.3,
            width:244
          }
        };
      } else {
        // redirect to login page
        this.router.navigate(['/login']);
      }
    }, (errorResp) => {
      console.error('Oops, something went wrong getting the logged in status')
    });
  }

  getTime(){
    var d = new Date();
    this.time = d.getTime();
  }

  resetQRCode(){
    this.qrcode = {
      apptype:"",
      duration:"",
      scene:"",
      model:"",
      structure:[],
      type:"",
      storageLocation:'1'
    };
  }

  saveqrcode() {

    this.formData = new FormData();
    //locate the file element meant for the file upload.
    let inputElFile: HTMLInputElement = this.qrcode.storageLocation=='1' ? this.el.nativeElement.querySelector('#file'):null;
    //locate the file element meant for the file upload.
    let inputElPhoto: HTMLInputElement = this.el.nativeElement.querySelector('#thumb');
    //get the total amount of files attached to the file input.
    let fileCountPhoto: number = inputElPhoto.files.length;

    if (fileCountPhoto > 0) {
      this.formData.append('photo', inputElPhoto.files.item(0));
      if(inputElFile)
        this.formData.append('file', inputElFile.files.item(0));
      var qrcodeString = JSON.stringify(this.qrcode);
      this.formData.append('qrcode', qrcodeString);
      this.http.post(MY_HOST + '/qrcode', this.formData)
        .subscribe((res:QRCodePushResponse) => {
            let id = res['_id'];
            this.QRCodeSavedEvent.emit();
            QRCode.toDataURL(MY_HOST + '/qrcode' + "/" + res._id, this.opts, function (err, url) {
              res.qrcode = url;
            })
            this.newqrcode = res;
            this.getTime();
            this.resetQRCode();
          }, (err) => {
            console.log(err);
          }
        );
    }
  }

  addMenuItem(event){
    this.qrcode.structure.push(
      {
        "mesh": "",
        "name": "test",
        "Description": "",
        "showLabel": false,
        "isAnimation": false,
        "animationTrigger": "",
        "steps":[],
        "children":[]
      }
    );
  }

  setType(type){
    this.qrcode.type = type;
    this.qrcodes = null;
    this.newqrcode = null;
  }
}
export interface QRCodePushResponse {
  apptype: string,
  description: string,
  duration: string,
  file_size: string,
  model: string,
  scene: string,
  skey: string,
  thumb: string,
  qrcode:string,
  title: string,
  type: string,
  updated_date: string,
  url: string,
  _id: string
}