import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { qrcodeCreateComponent } from './qrcode-create.component.ts';

describe('qrcodeCreateComponent', () => {
  let component: qrcodeCreateComponent;
  let fixture: ComponentFixture<qrcodeCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ qrcodeCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(qrcodeCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
