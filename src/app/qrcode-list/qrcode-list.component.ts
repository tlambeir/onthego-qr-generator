import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MY_HOST } from '../app.constants';
import *  as QRCode from 'qrcode'
import {MatDialog} from '@angular/material/dialog';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: './dialog-content-example-dialog.html',
})
export class DialogContentExampleDialog {}

@Component({
  selector: 'app-qrcode-list',
  templateUrl: './qrcode-list.component.html',
  styleUrls: ['./qrcode-list.component.css']
})
export class QrcodeListComponent implements OnInit {

  qrcodes: any;
  opts: any;
  currentPage:number;
  numOfResults:any;
  pages:any;
  constructor(private router: Router, private authService: AuthService, private http: HttpClient, public dialog: MatDialog) { }

  ngOnInit() {
    this.authService.getLoggedin().subscribe((resp: boolean) => {
      if(resp){
        this.currentPage=1;
        this.opts = {
          errorCorrectionLevel: 'H',
          type: 'image/jpeg',
          rendererOpts: {
            quality: 0.3,
            width:244
          }
        };
        this.load();
      }
      else {
        // redirect to login page
        this.router.navigate(['/login']);
      }
    });
  }

  openDialog(id) {
    const dialogRef = this.dialog.open(DialogContentExampleDialog);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if(result){
        this.http.delete(MY_HOST + '/qrcode/' +id).subscribe((data: CustomResponse) => {
          console.log(`Delete result: ${data}`);
          this.reload();
        });
      }
    });
  }

  reload(){
    this.load();
  }
  setPage(page: number) {
    // get pager object from service
    this.currentPage = page;

    // get current page of items
    this.load();
  }
  load(){
    this.http.get(MY_HOST + '/qrcode?page='+this.currentPage).subscribe((data: CustomResponse) => {
      this.qrcodes = data.qrcodes;
      this.currentPage = data.currentPage;
      this.numOfResults= data.numOfResults;
      this.pages= Array(data.pages).fill(0).map((x,i)=>i);
      for(var i=0; i < this.qrcodes.length; i++ ){
        var qrcode = this.qrcodes[i];
        QRCode.toDataURL(MY_HOST + '/qrcode' + "/" + qrcode._id, this.opts, function (err, url) {
          qrcode.qrcode = url;
        })
      }
    });
  }
}
export interface CustomResponse {
  qrcodes: any;
  currentPage:any;
  numOfResults:any;
  pages:any;
}
