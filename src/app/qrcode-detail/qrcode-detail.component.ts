import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { MenuitemDetailComponent } from '../menuitem-detail/menuitem-detail.component';
import { MY_HOST } from '../app.constants';

@Component({
  selector: 'app-qrcode-detail',
  templateUrl: './qrcode-detail.component.html',
  styleUrls: ['./qrcode-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class QrcodeDetailComponent implements OnInit {

  qrcode = {};

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.getQrcodeDetail(this.route.snapshot.params['id']);
  }

  deleteQrcode(id) {
    this.http.delete(MY_HOST + '/qrcode/'+id)
        .subscribe(res => {
              this.router.navigate(['/qrcodes']);
            }, (err) => {
              console.log(err);
            }
        );
  }

  getQrcodeDetail(id) {
    this.http.get(MY_HOST + '/qrcode/'+id).subscribe(data => {
      this.qrcode = data;
    });
  }

}