var saml2 = require('saml2-js');
var fs = require('fs');
var express = require('express');
var constants = require('../constants');
var router = express.Router();

//var host = "http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com:3000";

// Create service provider
var sp_options = {
  entity_id: constants.host + "/saml/metadata.xml",
  private_key: fs.readFileSync("saml/private_key.pem").toString(),
  certificate: fs.readFileSync("saml/certificate.crt").toString(),
  assert_endpoint: constants.host + "/assert"
};
var sp = new saml2.ServiceProvider(sp_options);

// Create identity provider
var idp_options = {
  sso_login_url: "https://bfed-qa.ext.net.nokia.com/idp/profile/Shibboleth/SSO",
  sso_logout_url: "https://bfed-qa.ext.net.nokia.com/idp/profile/SAML2/Redirect/SLO",
  certificates: [fs.readFileSync("saml/bfed-signing-qa.cer").toString()]
};
var idp = new saml2.IdentityProvider(idp_options);

router.get('/metadata.xml', function(req, res) {
  res.type('application/xml');
  res.send(sp.create_metadata());
});

// Starting point for login
router.get('/login', function(req, res) {
  sp.create_login_request_url(idp, {}, function(err, login_url, request_id) {
    if (err != null)
      return res.send(500);
    res.redirect(login_url);
  });
});

// Assert endpoint for when login completes
router.post("/assert", function(req, res) {
  var options = {request_body: req.body};
  sp.post_assert(idp, options, function(err, saml_response) {
    if (err != null)
      return res.send(500);

    // Save name_id and session_index for logout
    // Note:  In practice these should be saved in the user session, not globally.
    name_id = saml_response.user.name_id;
    session_index = saml_response.user.session_index;

    res.send("Hello #{saml_response.user.name_id}!");
  });
});

// Starting point for logout
router.get("/logout", function(req, res) {
  var options = {
    name_id: name_id,
    session_index: session_index
  };

  sp.create_logout_request_url(idp, options, function(err, logout_url) {
    if (err != null)
      return res.send(500);
    res.redirect(logout_url);
  });
});

module.exports = router;