var express = require('express');
var router = express.Router();

// Starting point for login

router.post('/login', function(req, res) {
  sess = req.session;
  sess.email = req.body.email;
  if(sess.email) {
    res.json(true);
  }
  else {
    res.json(false);
  }
});

router.get('/loggedin', function(req, res) {
  sess = req.session;
  if(sess.email) {
    res.json(true);
  }
  else {
    res.json(false);
  }
});

router.get('/logout',function(req, res) {
  req.session.destroy(function(err) {
    if(err) {
      res.json(false);
    } else {
      res.json(true);
    }
  });

});

module.exports = router;