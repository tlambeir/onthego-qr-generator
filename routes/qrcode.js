var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Qrcode = require('../models/Qrcode.js');
var MenuItem = require('../models/MenuItem.js');
var multer = require('multer');
var path = require('path');

/*var DIR = './uploads/';
var upload = multer({dest: DIR}).single('photo');*/

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});

var upload  = multer({ storage: storage }).fields([{
  name: 'photo', maxCount: 1
}, {
  name: 'file', maxCount: 1
}]);

/* GET ALL QRCODES */
router.get('/', function(req, res, next) {
  var resPerPage = req.query.resPerPage ? parseInt(req.query.results) : 10;
  var page = req.query.page ? parseInt(req.query.page) : 1;
  numOfQrcodes = Qrcode.count();
  Qrcode.count({}, function( err, count){
    Qrcode.find()
      .skip((resPerPage * page) - resPerPage)
      .limit(resPerPage)
      .sort('-updated_date')
      .exec(function(err, qrcodes) {
        if (err) return next(err);
        var result = {
          qrcodes:qrcodes,
          currentPage: page,
          pages: Math.ceil(count / resPerPage),
          numOfResults: count
        };
        res.json(result);
      });
  })

});

/* GET SINGLE QRCODE BY ID */
router.get('/:id', function(req, res, next) {
  Qrcode.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    MenuItem
      .find({ qrcode : req.params.id })
      .exec(function (err, structure) {
        if (err) return handleError(err);
        // returns all stories that have Bob's id as their author.
        post.structure = structure;

        if(structure.length > 0){
          for(var i=0;i < structure.length; i++) {
            findChildrenRecursive(structure[i], res, post);
          }
        } else {
          res.json(post);
        }
      });

  });
});

var count = 0;
function findChildrenRecursive(parent, res, post){
  count++;
  MenuItem
    .find({ parent : parent._id })
    .exec(function (err, children) {
      if (err) return handleError(err);
      parent.children = children;
      if(children.length > 0){
        for(var i=0;i < children.length; i++) {
          findChildrenRecursive(children[i], res, post);
        }
      }
      count --;
      if(count ===0){
        res.json(post);
      }
    });
}

/* SAVE QRCODE */
router.post('/', function(req, res, next) {

  upload(req, res, function (err) {
    if (err) {
      // An error occurred when uploading
      console.log(err);
      return res.status(422).send("an Error occured")
    }
    // No error occured.

    var structureArray = [];
    try {
      req.body.qrcode = JSON.parse(req.body.qrcode);
    }
    catch(err) {
      return res.status(422).send(err.message)
    }


    var root = "http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/onthego-qr-generator/";
    //var root = "C:\\Users\\lambeth\\PhpstormProjects\\onthego-qr-generator\\mean-angular5\\";
    if(req.files.photo)
      req.body.qrcode.thumb = root + req.files.photo[0].path;
    if(req.files.file)
      req.body.qrcode.url = root + req.files.file[0].path;

    for(var i=0;i < req.body.qrcode.structure.length; i++) {
      structureArray.push(req.body.qrcode.structure[i]);
    }
    req.body.qrcode.structure = [];

    if(req.body.qrcode.type){
      switch (req.body.qrcode.type){
        case 'video':
          req.body.qrcode.apptype = "Video";
          break;
        case 'url':
          req.body.qrcode.apptype = "URL";
          break;
        case 'threesixty':
          req.body.qrcode.apptype = "360 Video";
          break;
        case 'pdf':
          req.body.qrcode.apptype = "PDF";
          break;
        default:
          return res.status(422).send("Please provide a valid type.");
          break;
      }
    } else {
      return res.status(422).send("Please provide a valid type.");
    }


    Qrcode.create(req.body.qrcode, function (err, qrcode) {
      if (err) return next(err);
      for(var i=0;i < structureArray.length; i++) {
        var menuItem = structureArray[i];
        menuItem.qrcode = qrcode._id;
        processChildrenRecursive(menuItem);
      }
      res.json(qrcode);
    });


  });
});

function processChildrenRecursive(child){
  var childArray = [];
  for(var i=0;i < child.children.length; i++) {
    childArray.push(child.children[i]);
  }
  child.children = [];
  MenuItem.create(child, function (err, parent) {
    if (err) return next(err);
    for(var i=0;i < childArray.length; i++) {
      var menuItem = childArray[i];
      menuItem.parent = parent._id;
      processChildrenRecursive(menuItem);
    }
  });
}

function processUpdateChildrenRecursive(child){
  var childArray = [];
  for(var i=0;i < child.children.length; i++) {
    childArray.push(child.children[i]);
  }
  child.children = [];
  MenuItem.findByIdAndUpdate(child._id, child, function (err, parent) {
    if (err) return next(err);
    for(var i=0;i < childArray.length; i++) {
      var menuItem = childArray[i];
      menuItem.parent = parent._id;
      processUpdateChildrenRecursive(menuItem);
    }
  });
}

/* UPDATE QRCODE */
router.put('/:id', function(req, res, next) {

  var structureArray = [];
  if(req.body.structure && req.body.structure.length > 0){
    for(var i=0;i < req.body.structure.length; i++) {
      structureArray.push(req.body.structure[i]);
    }
  }
  req.body.structure = [];


  Qrcode.findByIdAndUpdate(req.params.id, req.body, function (err, qrcode) {
    if (err) return next(err);
    for(var i=0;i < structureArray.length; i++) {
      var menuItem = structureArray[i];
      menuItem.qrcode = qrcode._id;
      processUpdateChildrenRecursive(menuItem);
    }
    res.json(qrcode);
  });
});

/* DELETE QRCODE */
router.delete('/:id', function(req, res, next) {
  Qrcode.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;