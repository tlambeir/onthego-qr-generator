var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/User.js');
var multer = require('multer');
var path = require('path');


/* GET ALL USERS */
router.get('/', function(req, res, next) {
  var resPerPage = req.query.resPerPage ? parseInt(req.query.results) : 10;
  var page = req.query.page ? parseInt(req.query.page) : 1;
  numOfUsers = User.count();
  User.count({}, function( err, count){
    User.find()
      .skip((resPerPage * page) - resPerPage)
      .limit(resPerPage)
      .sort('-updated_date')
      .exec(function(err, users) {
        if (err) return next(err);
        var result = {
          users:users,
          currentPage: page,
          pages: Math.ceil(count / resPerPage),
          numOfResults: count
        };
        res.json(result);
      });
  })

});

/* GET SINGLE USER BY ID */
router.get('/:id', function(req, res, next) {
  User.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* SAVE USER */
router.post('/', function(req, res, next) {
  User.create(req.body, function (err, user) {
    if (err) return next(err);
    res.json(user);
  });
});

/* UPDATE USER */
router.put('/:id', function(req, res, next) {
  User.findByIdAndUpdate(req.params.id, req.body, function (err, user) {
    if (err) return next(err);
    res.json(user);
  });
});

/* DELETE USER */
router.delete('/:id', function(req, res, next) {
  User.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;