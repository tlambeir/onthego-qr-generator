var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var MenuItem = require('../models/MenuItem.js');

/* GET ALL QRCODES */
router.get('/', function(req, res, next) {
  MenuItem.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});

module.exports = router;