var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var MenuItemSchema = Schema({
  qrcode : { type: Schema.Types.ObjectId, ref: 'Qrcode' },
  parent : { type: Schema.Types.ObjectId, ref: 'MenuItem' },
  children:[{ type: Schema.Types.ObjectId, ref: 'MenuItem' }],
  name: String,
  Description: String,
  mesh: String,
  showLabel: Boolean,
  isAnimation:  Boolean,
  animationTrigger: String,
  steps: [],
  asset: String,
  assetType: String,
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('MenuItem', MenuItemSchema);