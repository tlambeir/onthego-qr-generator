var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LicenseSchema = new Schema({
  amount: Number,
  company : { type: Schema.Types.ObjectId, ref: 'Company' },
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('License', LicenseSchema);