var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
  email: String,
  roles:[{ type: Schema.Types.ObjectId, ref: 'Role' }],
  company : { type: Schema.Types.ObjectId, ref: 'Company' },
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('User', UserSchema);