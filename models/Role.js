var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = new Schema({
  name: String,
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Role', RoleSchema);