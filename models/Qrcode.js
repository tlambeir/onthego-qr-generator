var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QrcodeSchema = new Schema({
  title: String,
  description: String,
  skey: String,
  duration: String,
  type: String,
  apptype: String,
  scene: String,
  url: String,
  file_size: String,
  thumb: String,
  model: String,
  storageType: String,
  structure : [{ type: Schema.Types.ObjectId, ref: 'MenuItem' }],
  updated_date: { type: Date, default: Date.now },
  created_by: { type: Schema.Types.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Qrcode', QrcodeSchema);