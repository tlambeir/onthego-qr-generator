var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var CompanySchema = new Schema({
  name: String,
  updated_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Company', CompanySchema);